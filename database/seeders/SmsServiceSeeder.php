<?php

namespace Database\Seeders;

use App\Models\SmsService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SmsServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SmsService::create([
            'name'=>'eskiz',
            'token'=>'1122'
        ]);
    }
}
