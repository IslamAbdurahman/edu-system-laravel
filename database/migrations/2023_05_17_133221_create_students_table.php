<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('gender');
            $table->date('birth_date');
            $table->string('phone');
            $table->string('parent_phone');
            $table->tinyInteger('kitchen')->default(0);
            $table->tinyInteger('bedroom')->default(0);
            $table->integer('discount_education')->default(0);
            $table->integer('discount_kitchen')->default(0);
            $table->integer('discount_bedroom')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
};
