<?php

return [
    // main page //
    'title' => 'Привет',
    'page'=>'Страница',
    'sms_service'=>'SMS услуга',
    'email'=>'Электронная почта',
    'type_email'=>'Введите адрес электронной почты',
    'password'=>'Пароль',
    'type_password'=>'Введите пароль',
    'checking'=>'Проверка',
    'close'=>'Закрывать',
    'save'=>'Сохранять',
    'main_page'=>'Главная страница',
    'lids'=>'Крышки',
    'days'=>'дни',
    'expired'=>'Истекший',
    'search'=>'Поиск',
    'english'=>'Анг',
    'uzbek'=>'Уз',
    'russian'=>'Ру',
    'karakalpak'=>'Кк',
    'page_here'=>'Страница здесь',


    // sidebar //

    'dashboard'=>'Панель приборов',
    'profile'=>'Профиль',
    'admins'=>'Админы',
    'teachers'=>'Учителя',
    'workers'=>'Рабочие',
    'edit_profile'=>'Редактировать профиль',
    'log_out'=>'Выйти',
    'rooms'=>'Комната',
    'sciences'=>'Науки',
    'students'=>'Студенты',
    'courses'=>'Курсы',
    'groups'=>'Группы',
    'graphics'=>'Графика',
    'cashbox'=>'Касса',
    'payments'=>'Платежи',
    'salaries'=>'Заработная плата',
        'tests'=>'Тесты',
    'developer'=>'Программист',


    // Dashboard //
    'more_info'=>'Больше информации',
    'payment_graphics'=>'Платежная графика',
    'calendar'=>'Календарь',
    'group'=>'Группа',
    'paid'=>'Оплаченный',
    'remaining'=>'Оставшийся',


    // Admins //
    'name'=>'Имя',
    'phone'=>'Телефон',
    'image'=>'Изображение',
    'role'=>'Роль',
    'add'=>'Добавлять',
    'type_name'=>'Введите имя',
    'type_phone'=>'Тип телефона',
    'select'=>'Выбирать',
    'rights'=>'Права',
    'admin_update'=>'Обновление администратора',
    'delete'=>'Удалить',
    'your_profile'=>'Ваш профиль',
    'read_only'=>'Только чтение',
    'update'=>'Обновлять',
    'delete_message'=>'Вы уверены?',


    // Teachers //

    'balance'=>'Баланс',
    'science'=>'Наука',

    // room.show //

    'room'=>'Комната',
    'monday' => 'Понедельник',
    'tuesday' => 'Вторник',
    'wednesday' => 'Среда',
    'thursday' => 'Четверг',
    'friday' => 'Пятница',
    'saturday' => 'Суббота',
    'sunday' => 'Воскресенье',

    'send_sms_all_students'=>'Отправить смс всем ученикам',
    'sms'=>'SMS',
    'type_sms'=>'Пишите СМС',
    'send'=>'Отправлять',
    'gender'=>'Пол',
    'birthday'=>'День рождения',
    'discount_education'=>'Скидка',
    'parent_phone'=>'Родительский телефонe',
    'select_gender'=>'Выберите пол',
    'male'=>'Мужской',
    'female'=>'Женский',
    'type_parent_phone'=>'Введите родительский телефон',
    'percent'=>'Процент',
    'select_group'=>'Выберите группу',
    'select_lid'=>'Select lid',
    'comment'=>'Комментарий',
    'type_comment'=>'Введите комментарий',



    // Groups //

    'a_z'=>'А-Я',
    'z_a'=>'Я-А',
    'absent'=>'A',
    'reason'=>'R',
    'came'=>'C',

    'missed'=>'Пропущенный',

    'added_date'=>'Дата добавления',
    'attendance'=>'посещаемость',

    'level'=>'Уровень',
    'amount'=>'Количество',
    'teacher'=>'Учитель',
    'course'=>'Курс',
    'start'=>'Начинать',
    'end'=>'Конец',
    'type_level'=>'Уровень типа',
    'type_amount'=>'Type amount',
    'not_selected'=>'Не выбран',
    'teacher_percent'=>'Учитель процент',
    'add_graphics'=>'Добавить графику',
    'select_year'=>'Выберите год',
    'education'=>'Образование',
    'kitchen'=>'Кухня',
    'bedroom'=>'Спальня',


    // Graphics //

    'group_graphic_title'=>'Отправить смс этой группе : ',
    'group_graphic_text'=>'Сообщения будут отправлены выбранным членам группы.',
    'unfinished'=>'Незаконченный',
    'finished'=>'Законченный',
    'student'=>'Студент',
    'graph'=>'График',
    'month'=>'Месяц',
    'unpaid'=>'Неоплачиваемый',
    'send_sms'=>'Отправить смс',


    // cash box //

    'cash_cashbox'=>'Денежная касса',
    'card_cashbox'=>'Карточная касса',
    'click_cashbox'=>'Click касса',
    'personal'=>'Личный',


    // payments //

    'date'=>'Дата',
    'discount'=>'Скидка',
    'type_discount'=>'Тип скидки',
    'worker'=>'Рабочий',
    'user'=>'Пользователь',





    // Controllers //

    'saved'=>'Сохранено',
    'updated'=>'Обновлено',
    'deleted'=>'Удалено',
    'cannot_create'=>'Невозможно сохранить',
    'cannot_update'=>'Невозможно обновить',
    'cannot_delete'=>'Невозможно удалить',

    'student_registered'=>'Студент успешно зарегистрирован',

    'all_sms_sent'=>'Все отправленные СМС.',

    'sms_sent'=>'СМС отправлено.',

    'busy_room'=>'Комната занята в выбранное время. Группа не может быть сохранена',

    'sms_updated'=>'Обновлен сервис смс',
    'sms_invalid'=>'Неверный логин или пароль',


    'limit'=>'Платформа достигла максимального количества студентов',


    'date_zone'=>'ru-ru',

    'text'=>'Текст',
    'service'=>'Услуга',
    'status'=>'Статус',

    'test'=>'Тест',
    'result'=>'Результат',
    'type_result'=>'Введите результат',

    'reason_text'=>'Разумный',
    'absent_text'=>'Отсутствующий',

    'login_header'=>'Введите адрес электронной почты и пароль',
    'remember'=>'Помнить',
    'login'=>'Вход',
    'or'=>'ИЛИ',
    'forget_password'=>'Забыли пароль',

    'kassa_error'=>'Недостаточный баланс в кассе',
    'teacher_balance_error'=>'Недостаточный баланс учителя',

    'daily_timetable'=>'Ежедневное расписание',

    'upload_excel'=>'Выберите файл Excel'












];
