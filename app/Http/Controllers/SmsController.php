<?php

namespace App\Http\Controllers;

use App\Models\Sms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function Symfony\Component\String\s;

class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->per_page){
            $per_page = $request->per_page;
        }else{
            $per_page = 10;
        }
        if ($request->search && $request->from && $request->to){
            $search = $request->search;
            $from = $request->from;
            $to = $request->to;
            $sms = DB::table('sms as s')
                ->leftJoin('users as u','u.id','=','s.user_id')
                ->leftJoin('students as st','st.id','=','s.student_id')
                ->select('s.*','u.name as user','st.name as student')
                ->where('st.name','like', '%'.$request->search.'%')
                ->whereBetween('s.date',[$request->from,$request->to.' 23:59:59'])
                ->groupBy('s.id')
                ->orderBy('s.date','desc')
                ->paginate($per_page);
        }elseif ($request->from && $request->to){
            $search = '';
            $from = $request->from;
            $to = $request->to;
            $sms = DB::table('sms as s')
                ->leftJoin('users as u','u.id','=','s.user_id')
                ->leftJoin('students as st','st.id','=','s.student_id')
                ->select('s.*','u.name as user','st.name as student')
                ->whereBetween('s.date',[$request->from,$request->to.' 23:59:59'])
                ->groupBy('s.id')
                ->orderBy('s.date','desc')
                ->paginate($per_page);
        }elseif ($request->search){
            $search = $request->search;
            $from = date('Y-m-d');
            $to = date('Y-m-d');
            $sms = DB::table('sms as s')
                ->leftJoin('users as u','u.id','=','s.user_id')
                ->leftJoin('students as st','st.id','=','s.student_id')
                ->select('s.*','u.name as user','st.name as student')
                ->where('st.name','like', '%'.$request->search.'%')
                ->groupBy('s.id')
                ->orderBy('s.date','desc')
                ->paginate($per_page);
        }else {
            $search = '';
            $from = date('Y-m-d');
            $to = date('Y-m-d');
            $sms = DB::table('sms as s')
                ->leftJoin('users as u','u.id','=','s.user_id')
                ->leftJoin('students as st','st.id','=','s.student_id')
                ->select('s.*','u.name as user','st.name as student')
                ->groupBy('s.id')
                ->orderBy('s.date','desc')
                ->paginate($per_page);
        }

        return view('admin.sms.index',compact('sms',
            'search','per_page','from','to'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function show(Sms $sms)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function edit(Sms $sms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sms $sms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sms $sms)
    {
        try {

            $sms->delete();

            return redirect()->back()->withErrors([
                'success'=>__('lang.deleted'),
            ]);
        }catch (\Exception $exception){

            return redirect()->back()->withErrors([
                'error'=>__('lang.cannot_delete'),
            ]);
        }
    }
}
