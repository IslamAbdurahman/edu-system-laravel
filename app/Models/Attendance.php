<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'attendances';

    protected $fillable = [
        'student_id',
        'group_id',
        'date',
        'status'
    ];
}
