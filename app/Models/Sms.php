<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class Sms extends Model
{
    use HasFactory;
    public $timestamps=false;
    protected $table = 'sms';

    protected $fillable = [
        'student_id',
        'user_id',
        'text',
        'date',
        'service',
        'status',
    ];


    public static function eskiz($num, $message){
        $url = 'https://notify.eskiz.uz/api/message/sms/send';

        $service = SmsService::where('name','=','eskiz')->first();

        $res = Http::withToken($service->token)
            ->attach('mobile_phone', $num)
            ->attach('message', $message)
            ->post($url);

        return $res->body();
    }

    public static function sysdc($num, $message){
        $url = 'https://sms.sysdc.ru/index.php';

//        $token = DB::select("select * from sms_services where name = 'sysdc'");

        $res = Http::withToken('sn5iBAxAtK1ZVh0SlZx74ADA4DGz2aDZf5QknF0pfGzEvPWNJoBML94otvXgE4GB')
            ->attach('mobile_phone', $num)
            ->attach('message', $message)
            ->post($url);

        return $res->body();
    }

    public static function play_mobile($num, $message){

        $service = DB::select("select * from sms_services where name = 'play_mobile'");

        $data = [
            'recipient'=>$num,
            'message-id'=>'abc000000001',
            'sms'=> [
                'originator'=>'3700',
                'content'=>[
                    'text'=> $message
                ]
            ],
        ];

        $key = $service[0]->key;
        $secret = $service[0]->secret;
        $url = 'http://91.204.239.44/broker-api/send';

        $res = \Illuminate\Support\Facades\Http::withBasicAuth($key, $secret)
            ->post($url, [
                'messages' => $data
            ]);

        return $res->body();
    }


}
