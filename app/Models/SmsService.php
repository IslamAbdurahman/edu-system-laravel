<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsService extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'sms_services';

    protected $fillable = [
        'name',
        'token'
    ];
}
