<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    use HasFactory;
    public $timestamps=false;

    protected $table = 'salaries';
    protected $fillable = [
        'personal',
        'worker_id',
        'amount',
        'user_id',
        'kassa_id',
        'date',
        'comment',
    ];
}
