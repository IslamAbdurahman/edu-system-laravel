<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    public $timestamps=false;

    protected $table = 'payments';
    protected $fillable = [
        'month',
        'amount',
        'kitchen',
        'bedroom',
        'education',
        'comment',
        'user_id',
        'graphic_id',
        'student_id',
        'kassa_id',
        'date',
    ];
}
