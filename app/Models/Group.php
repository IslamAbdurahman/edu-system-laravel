<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    public $timestamps=false;

    protected $table = 'groups';
    protected $fillable = [
        'name',
        'level',
        'amount',
        'teacher_id',
        'course_id',
        'group_id',
        'days',
        'percent',
        'starts_at',
        'ends_at',
        'status',
    ];
}
