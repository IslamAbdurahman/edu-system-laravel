<?php

namespace App\Jobs;

use App\Imports\StudentsImport;
use App\Models\Students;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class ImportStudentsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $array;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($array)
    {
        $this->array = $array;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->array as $row){
            $students = Students::all()->count();
            $limit = limit_students();

            if ($limit > $students){
                $student = Students::firstOrCreate([
                    'name'=>$row[1],
                    'phone'=>$row[4],
                ]);

                if ($row[2] == 0 || $row[2] == 'female'){
                    $gender = 'female';
                }else{
                    $gender = 'male';
                }
                $student->gender = $gender;
                $student->birth_date = date('Y-m-d',strtotime($row[3]));
                $student->parent_phone = $row[5];

                $student->save();
            }else{
                break;
            }
        }

    }
}
